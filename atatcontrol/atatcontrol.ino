#include "I2Cdev.h"
#include "MPU6050.h"
#include "Wire.h"

const int mpuAddress = 0x68;  // Puede ser 0x68 o 0x69
MPU6050 mpu(mpuAddress);
 
int ax, ay, az;
int gx, gy, gz;

// Variables para control
const float WINDUP_GUARD_GAIN = 5.0;
float inclinacionActual = 0.0;
float inclinacionDeseada = 0.0;
float error = 0.0;
float errorAnterior = 0.0;
float errorTotal = 0.0;
//float kP = 6.0;
//float kI = 1.1;
//float kD = 30.0;
float kP = 0.1;
float kI = 0.0;
float kD = 0.0;
int potencia = 0;
float windupGuard = 0.0;
char constanteCambio = 'p';

// Variables Arduino
const int pinMotor = 9;

void setup(){
  Serial.begin(9600);
  Wire.begin();
  mpu.initialize();
  Serial.println(mpu.testConnection() ? F("IMU iniciado correctamente") : F("Error al iniciar IMU"));
}

void loop(){
   // Leer las aceleraciones 
   mpu.getAcceleration(&ax, &ay, &az);
 
   //Calcular los angulos de inclinacion
   float accel_ang_x = atan(ax / sqrt(pow(ay, 2) + pow(az, 2)))*(180.0 / 3.14);
   float accel_ang_y = atan(ay / sqrt(pow(ax, 2) + pow(az, 2)))*(180.0 / 3.14);
 
   // Mostrar resultados
   /*Serial.print(F("Inclinacion en X: "));
   Serial.print(accel_ang_x);
   Serial.print(F("\tInclinacion en Y:"));
   Serial.print(accel_ang_y);*/
    inclinacionActual = accel_ang_x;
   error = inclinacionDeseada - inclinacionActual;
   
   errorTotal = errorTotal + error;
   windupGuard = WINDUP_GUARD_GAIN;// / kI;
   if(errorTotal > windupGuard){
      errorTotal = windupGuard;
    }else if(errorTotal < -windupGuard){
      errorTotal = -windupGuard;
      //errorTotal = 0.0;
    }
  
   potencia = kP * error + kI * errorTotal + kD * (error-errorAnterior);

    if(potencia < 0){
      potencia = 0;
    }else if(potencia > 180){
      potencia = 180;  
    }
   
   errorAnterior = error;
   
   //Mostrar los valores por consola
   //Serial.print("Angle X: "); Serial.print(Angle[0]); Serial.print("\n");
   //Serial.print("Angle Y: "); Serial.print(Angle[1]); Serial.print("\n");
   Serial.print("Incli: ");
   Serial.print(accel_ang_x);
   Serial.print(" | ");
   Serial.print("Error: ");
   Serial.print(error);
   Serial.print(" | ");
   Serial.print("Error anterior: ");
   Serial.print(errorAnterior);
   Serial.print(" | ");
   Serial.print("Error total: ");
   Serial.print(errorTotal);
   Serial.print(" | ");
   Serial.print("kP: ");
   Serial.print(kP);
   Serial.print("kI: ");
   Serial.print(kI);
   Serial.print(" | ");
   Serial.print("kD: ");
   Serial.print(kD);
   Serial.print(" | ");
   Serial.print("Potencia: ");
   Serial.print(potencia);
   Serial.print(" | ");
   Serial.println();

   
   delay(10); //Nuestra dt sera, pues, 0.010, que es el intervalo de tiempo en cada medida
}
